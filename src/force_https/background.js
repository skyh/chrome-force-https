'use strict';


var FILTER = {
	urls: [
		"<all_urls>" // all urls matched to manifest permissions
	],
	types: [
		"main_frame"
	]
};


var FILTER_OPTIONS = [
	"blocking"
];


var HTTP_RX = /^http:/i;
var HTTPS = 'https:';


function onBeforeRequest(request) {
	var url = request.url;

	if (HTTP_RX.test(url)) {
		var redirectUrl = url.replace(HTTP_RX, HTTPS);

		return {
			redirectUrl: redirectUrl
		}
	}
}


chrome.webRequest.onBeforeRequest.addListener(onBeforeRequest, FILTER, FILTER_OPTIONS);
